'use strict'
const connectToDatabase = require('./db')
const { Survey } = require('./models/surveys')
const { Answers } = require('./models/answers')
const { User } = require('./models/users')
const OktaJwtVerifier = require('@okta/jwt-verifier')
const { authenticateAdmin, authenticateUser } = require('./authenticate')

const { activeSurvey, unactivatedSurvey, expiredSurvey } = require('./models/surveysIds')

require('dotenv').config({ path: './variables.env' })

function myCallback(callback, status, body) {
  if (status === 200) {
    return callback(null, {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin': '*' },
      body: JSON.stringify(body)
    })
  } else {
    return callback(null, {
      statusCode: status,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
  }
}

module.exports.getActiveSurveys = (req, res, callback) => {

  res.callbackWaitsForEmptyEventLoop = false
  connectToDatabase()
    .then(() => {
      authenticateAdmin(req.headers)
        .then(verifiedAdmin => {
          if (verifiedAdmin) {
            activeSurvey.findOne({ name: 'ids' })
              .then(activeSurveys => {
                Survey.find({
                  '_id': { $in: activeSurveys.surveyIds }
                })
                  .then(surveys => myCallback(callback, 200, surveys))
              })
          } else {
            authenticateUser(req.headers.Authorization)
              .then(verified => {
                if (verified) {
                  return true
                } else throw new Error
              })
              .then(() => {
                connectToDatabase()
                  .then(() => {
                    console.log('req headers', req.headers)
                    return User.findOne({ id: req.headers.User })
                      .then(user => {
                        if (user && user.surveysCompleted) {
                          return user.surveysCompleted
                        } else {
                          return []
                        }
                      })
                  })
                  .then(surveysCompleted => {
                    activeSurvey.findOne({ name: 'ids' })
                      .then(activeSurveys => {
                        const undoneSurveys = activeSurveys.surveyIds.filter(survey => !surveysCompleted.includes(survey))
                        Survey.find({
                          '_id': { $in: undoneSurveys }
                        })
                          .then(surveys => myCallback(callback, 200, surveys))
                      })
                  })
              })
              .catch(err => myCallback(callback, err.statusCode || 500, err))
          }
        })
    })

}

module.exports.getUnactivatedSurveys = (req, res, callback) => {

  res.callbackWaitsForEmptyEventLoop = false

  connectToDatabase()
    .then(() => {
      unactivatedSurvey.findOne({ name: 'ids' })
        .then(unactivatedSurveys => {
          Survey.find({
            '_id': { $in: unactivatedSurveys.surveyIds }
          })
            .then(surveys => myCallback(callback, 200, surveys))
            .catch(err => myCallback(callback, err.statusCode || 500, err))
        })
    })
}

module.exports.getExpiredSurveys = (req, res, callback) => {

  res.callbackWaitsForEmptyEventLoop = false
  console.log('it is here!')
  connectToDatabase()
    .then(() => {
      expiredSurvey.findOne({ name: 'ids' })
        .then(expiredSurveys => {
          Survey.find({
            '_id': { $in: expiredSurveys.surveyIds }
          })
            .then(surveys => myCallback(callback, 200, surveys))
            .catch(err => myCallback(callback, err.statusCode || 500, err))
        })
    })
}

module.exports.getOneSurvey = (req, res, callback) => {
  res.callbackWaitsForEmptyEventLoop = false
  console.log(req.pathParameters.id)

  connectToDatabase()
    .then(() => {
      Survey.findOne({ _id: req.pathParameters.id })
        .then(survey => {
          if (!survey) {
            myCallback(callback, 200, [])
          }
          else {
            myCallback(callback, 200, survey)
          }
        })
        .catch(err => myCallback(callback, err.statusCode || 500, err))
    })

}

module.exports.expireSurvey = (req, res, callback) => {

  const id = req.pathParameters.id
  res.callbackWaitsForEmptyEventLoop = false
  console.log(req)
  activeSurvey.update({ name: 'ids' }, { $pull: { surveyIds: id } }, (err, docs) => { if (err) console.log(err) })
    .then(() => {
      expiredSurvey.update({ name: 'ids' }, { $push: { surveyIds: id } }, { upsert: true, new: true, runValidators: true })
        .then(survey => myCallback(callback, 200, survey))
    })
    .catch(err => myCallback(callback, err.statusCode || 500, err))
}

module.exports.deleteSurvey = (req, res, callback) => {
  const id = req.pathParameters.id
  res.callbackWaitsForEmptyEventLoop = false
  connectToDatabase()
    .then(() => {
      Survey.findOneAndRemove({ _id: id })
        .then(survey => {
          console.log(survey)
          if (!survey) {
            myCallback(callback, 500, 'No survey could be deleted')
          }
          else {
            unactivatedSurvey.update({ name: 'ids' }, { $pull: { surveyIds: id } }, (err, docs) => { if (err) console.log(err) })
            expiredSurvey.update({ name: 'ids' }, { $pull: { surveyIds: id } }, (err, docs) => { if (err) console.log(err) })
            activeSurvey.update({ name: 'ids' }, { $pull: { surveyIds: id } }, (err, docs) => { if (err) console.log(err) })
            myCallback(callback, 200, survey)
          }
        })
        .catch(err => myCallback(callback, err.statusCode || 500, err))
    })
}

module.exports.activateSurvey = (req, res, callback) => {
  res.callbackWaitsForEmptyEventLoop = false
  const id = req.pathParameters.id

  console.log('id?', id)

  unactivatedSurvey.update({ name: 'ids' }, { $pull: { surveyIds: id } }, (err, docs) => { if (err) console.log(err) })
    .then(() => {
      activeSurvey.update({ name: 'ids' }, { $push: { surveyIds: id } }, { upsert: true, new: true, runValidators: true })
        .then(survey => myCallback(callback, 200, survey))
    })
    .catch(err => myCallback(callback, err.statusCode || 500, err))
}

module.exports.postNewSurvey = (req, res, callback) => {
  console.log(req.headers)
  res.callbackWaitsForEmptyEventLoop = false

  const surveyData = JSON.parse(req.body)
  const name = surveyData.name
  const created = surveyData.created
  const startDate = surveyData.startDate
  const expiresDate = surveyData.expiresDate
  const questions = surveyData.questions

  const newSurvey = new Survey({ name, created, startDate, expiresDate, questions })

  console.log(newSurvey)

  connectToDatabase()
    .then(() => {
      console.log('getting inside ctd .then()')
      newSurvey.save()
        .then(survey => {
          console.log('survey')
          if (startDate < Date.now()) {
            activeSurvey.update({ name: 'ids' }, { $push: { surveyIds: survey._id } }, (err, info) => {
              err ? myCallback(callback, err.statusCode || 500, err) : myCallback(callback, 200, survey)
            })
          } else {
            unactivatedSurvey.update({ name: 'ids' }, { $push: { surveyIds: survey._id } }, (err, info) => {
              err ? myCallback(callback, err.statusCode || 500, err) : myCallback(callback, 200, survey)
            })
          }

        })
        .catch(err => myCallback(callback, err.statusCode || 500, err))
    })
}

module.exports.answerSurvey = (req, res, callback) => {
  res.callbackWaitsForEmptyEventLoop = false
  console.log(req)
  const answersData = JSON.parse(req.body)

  const userId = answersData.userId
  const surveyId = answersData.surveyId
  const answers = answersData.answers

  const surveyResponse = new Answers({
    userId,
    surveyId,
    answers
  })

  console.log('survey response', surveyResponse)
  authenticateUser(req.headers.Authorization)
    .then(verified => {
      if (verified) {
        return true
      } else throw new Error('user not authenticated')
    })
    .then(() => {
      return connectToDatabase()
    })
    .then(() => {
      return surveyResponse.save()
    })
    .then(answers => {
      if (answers) {
        console.log(answers)
        User.findOneAndUpdate({ id: userId }, { $push: { surveysCompleted: surveyId } }, { upsert: true, new: true, setDefaultsOnInsert: true })
          .then(doc => console.log('doc ', doc))
      } else throw new Error('unable to save your answers')
      return
    })
    .then(() => myCallback(callback, 200, surveyResponse))
    .catch(err => myCallback(callback, err.statusCode || 500, err))
}

module.exports.getTakenSurveys = (req, res, callback) => {

  res.callbackWaitsForEmptyEventLoop = false
  console.log('confirming taken runs', req.headers)

  authenticateUser(req.headers.Authorization)
    .then(verified => {
      if (verified) {
        return true
      } else throw new Error
    })
    .then(() => {
      return connectToDatabase()
    })
    .then(() => {
      return User.findOne({ id: req.headers.User })
        .then(user => {
          console.log('the user', user)
          if (user && user.surveysCompleted) return user.surveysCompleted
          else return []
        })
    })
    .then(surveysCompleted => {
      console.log('complete', surveysCompleted)
      return Survey.find({
        '_id': { $in: surveysCompleted }
      })
    })
    .then(takenSurveysData => myCallback(callback, 200, takenSurveysData))
    .catch(err => myCallback(callback, err.statusCode || 500, err))
}

module.exports.getOneAnswers = (req, res, callback) => {

  res.callbackWaitsForEmptyEventLoop = false
  console.log('confirming taken runs', req.headers)
  const surveyId = req.pathParameters.id
  let surveyAnswers
  authenticateUser(req.headers.Authorization)
    .then(verified => {
      if (verified) {
        return true
      } else throw new Error
    })
    .then(() => {
      return connectToDatabase()
    })
    .then(() => {
      return Answers.findOne({ 'surveyId': surveyId })

    })
    .then(data => {
      surveyAnswers = data.answers
      return Survey.findOne({ '_id': surveyId })
    })
    .then(survey => {
      const res = surveyAnswers.map(answer => {
        let x = survey.questions.find(question => {
          return question._id == answer.questionId
        }
        )
        answer.title = x.title
        return answer
      })
      return res
    })
    .then(answers => myCallback(callback, 200, answers))
    .catch(err => myCallback(callback, err.statusCode || 500, err))

}

module.exports.getResults = (req, res, callback) => {
  // let response = []
  res.callbackWaitsForEmptyEventLoop = false
  authenticateAdmin(req.headers)
    .then(verifiedAdmin => {
      console.log('verified...', verifiedAdmin)
      if (verifiedAdmin) {
        connectToDatabase()
          .then(() => {
            Survey.find({})
              .then(async surveys => {
                // variable names promises common for Promise.all()
                const promises = surveys.map(async (survey) => {
                  const responses = await Answers.find({ 'surveyId': survey._id }, { lean: true })
                  return { survey, responses } // object with survey info and array of responses
                })
                const results = await Promise.all(promises)
                myCallback(callback, 200, results)
              })
              .catch(e => myCallback(callback, 500, e))
          })
      } else myCallback(callback, 500, 'You are not authorized!')
    })
}