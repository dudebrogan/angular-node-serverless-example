const OktaJwtVerifier = require('@okta/jwt-verifier')




const authenticateAdmin = (authorization) => {

    const authHeader = authorization.Authorization || '';
    const match = authHeader.match(/Bearer (.+)/);

    const oktaJwtVerifier = new OktaJwtVerifier({
        issuer: 'http://dev-973041.oktapreview.com/oauth2/default'
    })

    if (!match) {
        return false
    }

    const accessToken = match[1];
    return oktaJwtVerifier.verifyAccessToken(accessToken)
        .then(jwt => {
            return true
        })
        .then(() => {
            return fetch(`https://dev-973041.oktapreview.com/api/v1/users/${authorization.User}/groups`, {
                method: "GET",
                headers: {
                    "Content-type": "application/json",
                    "Accept": "application/json",
                    "Authorization": "SSWS 00eScxlIxfF3lFue0l6enfdpWuOvFlQoO1gPf_dyah"
                }
            })
        })
        .then(res => res.json())
        .then(body => {
            return body.some(group => {
                return group.profile.name === 'admin'
            })
        })
        .catch(err => {
            return false
            // res.status(401).send(err.message);
        });
}

const authenticateUser = (headerAuth) => {
    const authHeader = headerAuth || '';
    const match = authHeader.match(/Bearer (.+)/);

    const oktaJwtVerifier = new OktaJwtVerifier({
        issuer: 'http://dev-973041.oktapreview.com/oauth2/default'
    })

    if (!match) {
        return false
    }
    const accessToken = match[1]
    return oktaJwtVerifier.verifyAccessToken(accessToken)
        .then(() => { return true })
        .catch(() => { return false })
}

module.exports = { authenticateAdmin, authenticateUser }