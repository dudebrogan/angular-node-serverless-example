const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const correctTypeOfQuestion = (questionType) => {
    const questionOptions = ['Multiple Choice', 'Choose All That Apply', 'Free-Form', 'Yes/No', 'True/False']
    if (!questionOptions.includes(questionType)) return false

    return true
}

const Question = new Schema({
    title: {
        type: String,
        trim: true,
        required: true
    },
    questionType: {
        type: String,
        required: true,
        validate: [correctTypeOfQuestion, 'you must add a Multiple Choice, Choose All That Apply, Free Form, Yes/No, True/False question type']
    },
    possibleAnswers: {
        type: Array
    },
})

const surveySchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        trim: true
    },
    creator: {
        type: String,
        default: '1'
    },
    created: {
        type: Number,
        required: true
    },
    startDate: {
        type: Number,
        required: true
    },
    expiresDate: {
        type: Number,
        required: true
    },
    questions: [Question]
})

const Survey = mongoose.model('survey', surveySchema)

module.exports = {
    Survey,
    Question
}