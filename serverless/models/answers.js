const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const answersSchema = new Schema({
    userId: String,
    surveyId: String,
    answers: []
})

const Answers = mongoose.model('answers', answersSchema)

module.exports = { Answers }
