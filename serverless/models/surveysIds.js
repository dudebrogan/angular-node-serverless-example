const mongoose = require('mongoose');
// const Question = require('./question')

const Schema = mongoose.Schema;

const surveyId = new Schema({
    name: String,
    surveyIds: [{
        unique: true,
        type: String,
    }]
})

const activeSurvey = mongoose.model('ActiveSurvey', surveyId)
const unactivatedSurvey = mongoose.model('UnactivatedSurvey', surveyId)
const expiredSurvey = mongoose.model('ExpiredSurvey', surveyId)


module.exports = { activeSurvey, unactivatedSurvey, expiredSurvey }