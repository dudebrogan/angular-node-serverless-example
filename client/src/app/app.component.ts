import { Component } from '@angular/core'
import { OktaAuthService } from '@okta/okta-angular'
import { AuthService } from './services/auth.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isAuthenticated: boolean

  constructor(public oktaAuthService: OktaAuthService, private authService: AuthService, private router: Router) {
    // Subscribe to authentication state changes
  }

  async ngOnInit() {
    // this.router.navigate([''])
    this.oktaAuthService.getUser()
      .then(user => {
        if (user === undefined) {
          this.isAuthenticated = false
          this.oktaAuthService.isAuthenticated().then(res => {
            if (res === true)
              this.oktaAuthService.logout('/')
          })
        }
        else this.isAuthenticated = true
        console.log(`user ${user}`)
      })
    // this.isAuthenticated = await this.oktaAuthService.isAuthenticated() seems to return true when user is undefined? unsure why...
    //manually controlling, todo figure out why Okta is bad
    this.authService.setUserStatus()
    this.oktaAuthService.$authenticationState.subscribe(
      (isAuthenticated: boolean) => {
        this.authService.setUserStatus()

        this.isAuthenticated = isAuthenticated
        this.oktaAuthService.getAccessToken()
          .then(token => {
            console.log('token', token)
            localStorage.setItem('bearer', token)
          })
        this.oktaAuthService.getUser()
          .then(user => console.log(`user2 ${user}`))

      })
    this.oktaAuthService.getAccessToken()
      .then(token => {
        localStorage.setItem('bearer', token)
      })

    if (this.oktaAuthService.isAuthenticated()) {
      const accessToken = this.oktaAuthService.getAccessToken()

      const userInfo = this.oktaAuthService.getIdToken()

    }
  }

  updateAuthentication(isAuthenticated) {
    console.log('test4', isAuthenticated)
    this.isAuthenticated = isAuthenticated
    this.authService.setUserStatus()
  }
}
