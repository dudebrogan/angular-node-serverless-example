import { Routes, RouterModule } from '@angular/router'

import { NgModule } from '@angular/core';
import { NormiesComponent } from './normies.component';
import { TakeSurveyComponent } from '../take-survey/take-survey.component'
import { UserAuthGuard } from '../services/user.auth.service';

const routes: Routes = [
    {
        path: '',
        component: NormiesComponent,
        canActivate: [UserAuthGuard],
        children: [
            {
                path: '',
                children: [
                    { path: 'take', component: TakeSurveyComponent },
                    { path: '', pathMatch: 'full', redirectTo: 'take' },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class NormiesRoutes { }