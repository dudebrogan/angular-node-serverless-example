import { Component } from '@angular/core';
import { UserAuthGuard } from '../services/user.auth.service';

@Component({
  template: `
    <router-outlet></router-outlet>
  `,
  providers: [
    UserAuthGuard
  ]
})
export class NormiesComponent {
}