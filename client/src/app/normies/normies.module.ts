import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { NormiesComponent } from './normies.component';
import { MatDialogModule, MatGridListModule, MatRadioModule, MatIconModule, MatListModule, MatCheckboxModule, MAT_CHECKBOX_CLICK_ACTION, MatButtonModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatSelectModule } from '@angular/material'
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AnswerQuestionComponent } from '../take-survey/answer-question/answer-question.component';
import { NormiesRoutes } from './normies.routes'
import { ReactiveFormsModule } from '@angular/forms';
import { UserAuthGuard } from '../services/user.auth.service';
import { AuthService } from '../services/auth.service';
import { TakenSurveysComponent } from '../take-survey/taken-surveys/taken-surveys.component'
import { TakeSurveyComponent } from '../take-survey/take-survey.component';
import { SurveyanswersComponent } from '../components/surveyanswers/surveyanswers.component';

@NgModule({
    declarations: [
        NormiesComponent,
        TakeSurveyComponent,
        AnswerQuestionComponent,
        TakenSurveysComponent,
        SurveyanswersComponent,
    ],
    exports: [
        RouterModule,
        TakeSurveyComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatDialogModule,
        MatButtonModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule,
        MatSelectModule,
        MatCheckboxModule,
        MatListModule,
        MatIconModule,
        MatRadioModule,
        MatGridListModule,
        NormiesRoutes
    ],
    entryComponents: [
        SurveyanswersComponent
    ],
    providers: [
        { provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'noop' },
    ]
})

export class NormiesModule { }