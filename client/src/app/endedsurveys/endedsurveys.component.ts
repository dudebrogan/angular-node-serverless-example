import { Component, OnInit, Inject } from '@angular/core'
import { ExpiredSurveyService, surveys } from '../services/expiredsurveys.service'
import { SurveysService } from '../services/surveys.service'

import { MatDialog } from '@angular/material'
import { QuestionsDialog } from '../components/surveyquestions/surveyquestions.component'

@Component({
  selector: 'app-endedsurveys',
  templateUrl: './endedsurveys.component.html',
  styleUrls: ['./endedsurveys.component.scss'],
  providers: [ExpiredSurveyService, SurveysService]
})
export class EndedsurveysComponent implements OnInit {
  endedSurveys: any[] = []

  constructor(private expiredSurveyService: ExpiredSurveyService, public dialog: MatDialog, private surveysService: SurveysService) { }

  ngOnInit() {
    this.getSurveys()
  }

  openDialog(survey): void {
    let x = []
    this.dialog.open(QuestionsDialog, {
      width: '1000px',
      data: survey.questions
    })
  }

  endSurvey(surveyId) {
    console.log(surveyId)
    this.surveysService.makeSurveyExpired(surveyId)
      .subscribe(data => {
        console.log(data)
        this.getSurveys()
      })
  }

  deleteSurvey(surveyId) {
    console.log(surveyId)
    this.surveysService.deleteSurvey(surveyId)
      .subscribe(data => {
        console.log(data)
        this.getSurveys()
      })
  }

  getSurveys() {
    this.expiredSurveyService.getExpiredSurveys()
      .subscribe((data: any[]) => {
        let formattedData = data.map(survey => {
          survey.created = new Date(survey.created).toDateString()
          survey.startDate = new Date(survey.startDate).toDateString()
          survey.expiresDate = new Date(survey.expiresDate).toDateString()
          survey.questions = survey.questions.map(question => {
            question.possibleAnswers = question.possibleAnswers.map(answer => answer.text)
            return question
          })
          return survey
        })
        this.endedSurveys = formattedData
      })
  }

}
