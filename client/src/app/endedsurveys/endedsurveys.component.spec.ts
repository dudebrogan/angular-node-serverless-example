import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { EndedsurveysComponent } from './endedsurveys.component'

describe('EndedsurveysComponent', () => {
  let component: EndedsurveysComponent
  let fixture: ComponentFixture<EndedsurveysComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EndedsurveysComponent]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(EndedsurveysComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
