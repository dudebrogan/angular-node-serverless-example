import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ActivesurveysComponent } from './activesurveys.component'

describe('ActivesurveysComponent', () => {
  let component: ActivesurveysComponent
  let fixture: ComponentFixture<ActivesurveysComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActivesurveysComponent]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivesurveysComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
