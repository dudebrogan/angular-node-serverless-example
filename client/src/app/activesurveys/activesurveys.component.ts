import { Component, OnInit, Inject } from '@angular/core'
import { ActiveSurveyService, surveys } from '../services/activesurveys.service'
import { SurveysService } from '../services/surveys.service'

import { MatDialog } from '@angular/material'
import { QuestionsDialog } from '../components/surveyquestions/surveyquestions.component'

@Component({
  selector: 'app-activesurveys',
  templateUrl: './activesurveys.component.html',
  styleUrls: ['./activesurveys.component.scss'],
  providers: [ActiveSurveyService, SurveysService]
})
export class ActivesurveysComponent implements OnInit {
  activeSurveys: any[] = []
  getSurveysError = false

  constructor(private activeSurveyService: ActiveSurveyService, public dialog: MatDialog, private surveysService: SurveysService) { }

  ngOnInit() {
    this.getSurveys()
  }

  openDialog(survey): void {
    let x = []
    this.dialog.open(QuestionsDialog, {
      width: '1000px',
      data: survey.questions
    })
  }

  endSurvey(surveyId) {
    console.log(surveyId)
    this.surveysService.makeSurveyExpired(surveyId)
      .subscribe(data => {
        console.log(data)
        this.getSurveys()
      })
  }

  runResults() {
    this.surveysService.getResults()
      .subscribe(data => {
        console.log('data', data)
      })
  }

  deleteSurvey(surveyId) {
    console.log(surveyId)
    this.surveysService.deleteSurvey(surveyId)
      .subscribe(data => {
        console.log(data)
        this.getSurveys()
      })
  }

  getSurveys() {
    this.activeSurveyService.getActiveSurveys()
      .subscribe((data: any) => {
        let formattedData = data.map(survey => {
          survey.created = new Date(survey.created).toDateString()
          survey.startDate = new Date(survey.startDate).toDateString()
          survey.expiresDate = new Date(survey.expiresDate).toDateString()
          survey.questions = survey.questions.map(question => {
            question.possibleAnswers = question.possibleAnswers.map(answer => answer.text)
            return question
          })
          return survey
        })
        this.activeSurveys = formattedData
      }, error => {
        this.getSurveysError = error ? true : false
      })
  }
}

