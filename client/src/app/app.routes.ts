import { RouterModule, Routes } from '@angular/router';
import { SplashComponent } from './splash/splash.component';
// import { ActivesurveysComponent } from './activesurveys/activesurveys.component';
// import { EndedsurveysComponent } from './endedsurveys/endedsurveys.component';
// import { UnactivatedsurveysComponent } from './unactivatedsurveys/unactivatedsurveys.component';
// import { AddsurveyComponent } from './addsurvey/addsurvey.component';
import { OktaAuthModule, OktaCallbackComponent } from '@okta/okta-angular';
import { AdminAuthGuard } from './services/admin.auth.service';
import { NgModule } from '@angular/core';


const routes: Routes = [
  {
    path: 'home',
    component: SplashComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: '#',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
  },
  {
    path: 'implicit/callback',
    component: OktaCallbackComponent
  },
  {
    path: 'surveys',
    loadChildren: './normies/normies.module#NormiesModule'
  },
  {
    path: '**',
    redirectTo: '',
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutes { }