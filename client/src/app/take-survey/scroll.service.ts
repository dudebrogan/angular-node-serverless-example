import { Injectable } from '@angular/core';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

@Injectable()
export class MyScrollService {

    constructor(private scrollToService: ScrollToService) { }

    triggerScrollTo(id) {
        console.log('id', id)
        const config: ScrollToConfigOptions = {
            target: id
        };

        this.scrollToService.scrollTo(config);
    }
}