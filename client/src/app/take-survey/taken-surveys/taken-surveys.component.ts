import { Component, OnInit } from '@angular/core';
import { TakenSurveysService } from '../../services/takensurveys.service';
import { MatDialog } from '@angular/material';
import { SurveyanswersComponent } from '../../components/surveyanswers/surveyanswers.component';

@Component({
  selector: 'app-taken-surveys',
  templateUrl: './taken-surveys.component.html',
  styleUrls: ['./taken-surveys.component.scss'],
  providers: [TakenSurveysService]
})
export class TakenSurveysComponent implements OnInit {
  takenSurveys
  surveyAnswers

  constructor(private takenSurveyService: TakenSurveysService, public answersDialog: MatDialog) { }

  ngOnInit() {
    this.getTakenSurveys()
  }

  getTakenSurveys() {
    this.takenSurveyService.getTakenSurveys()
      .subscribe((data: any) => {
        let formattedData = data.map(survey => {
          survey.created = new Date(survey.created).toDateString()
          survey.startDate = new Date(survey.startDate).toDateString()
          survey.expiresDate = new Date(survey.expiresDate).toDateString()
          return survey
        })
        this.takenSurveys = formattedData
      })
  }

  clickTakenSurvey(id, index) {
    console.log(id, index)
    this.takenSurveyService.getOneAnswers(id)
      .subscribe(data => {
        console.log(data)
        this.surveyAnswers = data
        this.openDialog(data)
      })
  }

  openDialog(answers): void {
    console.log(answers)
    this.answersDialog.open(SurveyanswersComponent, {
      width: '1000px',
      data: answers
    });
  }

}
