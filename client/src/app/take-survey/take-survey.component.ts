import { Component, OnInit } from '@angular/core';
import { ActiveSurveyService } from '../services/activesurveys.service';
import { Survey } from '../models/surveyModels';
import { FormGroup, FormControl } from '@angular/forms';
import { AnswerSurveyService } from '../services/answerSurvey.service';
import { AuthService } from '../services/auth.service';
import { MyScrollService } from './scroll.service';

@Component({
  selector: 'app-take-survey',
  templateUrl: './take-survey.component.html',
  styleUrls: ['./take-survey.component.scss'],
  providers: [ActiveSurveyService, AnswerSurveyService, MyScrollService]
})
export class TakeSurveyComponent implements OnInit {
  activeSurveys: any[] = []
  activeSurveyIndex = -1
  activeSurvey: any
  form: FormGroup

  constructor(private activeSurveyService: ActiveSurveyService, private answerSurveyService: AnswerSurveyService, private authService: AuthService, private scrollToService: MyScrollService) { }

  ngOnInit() {
    this.getSurveys()
  }

  getSurveys() {
    this.activeSurveyService.getActiveSurveys()
      .subscribe((data: any) => {
        console.log('data', data)
        let formattedData = data.map(survey => {
          survey.created = new Date(survey.created).toDateString()
          survey.startDate = new Date(survey.startDate).toDateString()
          survey.expiresDate = new Date(survey.expiresDate).toDateString()
          return survey
        })
        this.activeSurveys = formattedData

      });
  }

  scrollTo(index) {
    this.scrollToService.triggerScrollTo(index)
  }

  submitSurvey() {
    const answers = Object.entries(this.form.value).map(answer => {
      console.log(answer)
      return {
        questionId: answer[0],
        answerValue: answer[1]
      }
    })

    console.log(this.form.value)
    const submission = {
      userId: this.authService.getUserId(),
      surveyId: this.activeSurvey._id,
      answers
    }

    console.log('sumision', submission)
    this.answerSurveyService.submitSurvey(submission)
      .subscribe(data => {
        if (data) {
          this.getSurveys()
          this.cancelSurvey()
        }
      })
  }

  clickSurvey(id, index) {
    this.activeSurvey = this.activeSurveys.find(survey => survey._id === id)
    this.activeSurveyIndex = index
    this.form = this.answerSurveyService.toFormGroup(this.activeSurvey.questions)
  }

  cancelSurvey() {
    this.activeSurvey = null
    this.activeSurveyIndex = -1
  }

}
