import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { QuestionBase } from '../../models/answerQuestionModels';
import { FormGroup, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-answer-question',
  templateUrl: './answer-question.component.html',
  styleUrls: ['./answer-question.component.scss']
})
export class AnswerQuestionComponent implements OnInit {
  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  @Input() index: Number;
  @Output() scrollToIndex = new EventEmitter()

  selectAnswer: string;
  chooseAllValid = false

  get isValid() {
    console.log('get isvalid', this.form.controls[this.question._id])
    this.form.controls[this.question._id]
    if (this.chooseAllValid) {
      return this.form.controls[this.question._id].valid
    } else return false
  }
  constructor() { }

  ngOnInit() {
    // console.log(this.question)
  }

  timeToScroll(index) {
    console.log(index)
    this.scrollToIndex.emit(index)
  }

  updateChooseAll(answer, index) {
    const value = (<FormArray>this.form.controls[this.question._id])
    let newValue = value.value //TODO: figure
    let newControls = value.controls
    console.log(value)

    if (newValue.includes(answer)) {
      const index = newValue.indexOf(answer)

      newControls = newControls.filter(ans => ans !== answer)
      newValue.splice(index, 1)
    } else {
      newControls.push(new FormControl(answer))
      newValue.push(answer)
    }
  }

}
