import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';

import SurveysUrl from '../../surveysurl'

import { QuestionBase, FreeFormQuestion, TrueFalseQuestion, ChooseAllThatApplyQuestion, MultipleChoiceQuestion, YesNoQuestion } from '../models/answerQuestionModels';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class AnswerSurveyService {
    headers

    constructor(private fb: FormBuilder, private http: HttpClient) {
        this.headers = new HttpHeaders({
            'Authorization': 'Bearer ' + localStorage.getItem('bearer')
        })
    }

    toFormGroup(questions: QuestionBase<any>[]) {
        let group: any = {};
        // console.log(questions)
        const formattedQuestions = this.formatQuestions(questions)
        // console.log(formattedQuestions)

        formattedQuestions.forEach(question => {
            // group[question._id] = question.required ? new FormControl(question.value || '', Validators.required)
            //     : new FormControl(question.value || '');

            // console.log(group)
            if (question.questionType == 'Choose All That Apply') {
                console.log('choose all')
                console.log(question)
                let formControls: FormControl[] = []
                // console.log('how undefined', )
                question.possibleAnswers.forEach(x => formControls.push(new FormControl()))
                const formArray = new FormArray(formControls, Validators.required)
                // question.possibleAnswers.forEach(x => formArray.push(new FormControl(false)))
                group[question._id] = this.fb.array([])
            } else {
                group[question._id] = new FormControl(question.value || '', Validators.required)
            }
        })

        const testForm = this.fb.group(group)


        // console.log('new group')
        // console.log(group)
        return new FormGroup(group);
    }

    formatQuestions(questions: any[]) {
        // console.log(questions)
        return questions.map(question => {
            let returnValue

            switch (question.questionType) {
                case 'Choose All That Apply':
                    returnValue = new ChooseAllThatApplyQuestion(question)
                    break

                case 'Yes/No':
                    returnValue = new YesNoQuestion(question)
                    break

                case 'True/False':
                    returnValue = new TrueFalseQuestion(question)
                    break

                case 'Multiple Choice':
                    returnValue = new MultipleChoiceQuestion(question)
                    break

                case 'Free Form':
                    returnValue = new FreeFormQuestion(question)
                    break

                default:
                    returnValue = new FreeFormQuestion(question)
                    break
            }
            return returnValue
        })
        // console.log(formattedQuestions)
    }

    submitSurvey(submission) {
        console.log('in service')
        console.log(submission)
        //find if can query question based on sub schema
        return this.http.post(`${SurveysUrl}takesurvey/${submission.surveyId}`, submission, { headers: this.headers })
            .pipe(
                retry(3),
                catchError(this.handleError)
            )
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };
}