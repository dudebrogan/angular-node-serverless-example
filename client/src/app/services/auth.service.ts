import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as OktaSignIn from '@okta/okta-signin-widget';

import * as okta from './../../oktaAuth'

import { catchError, retry, map, toArray } from 'rxjs/operators';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { OktaAuthService } from '@okta/okta-angular';

@Injectable()
export class AuthService {
    widget
    headers;
    isAdmin = new BehaviorSubject<boolean>(false)
    isUser = new BehaviorSubject<boolean>(null)
    userId

    constructor(public oktaAuthService: OktaAuthService, private http: HttpClient, ) {
        this.headers = new HttpHeaders({
            'Authorization': okta.oktaSSWS

        })

        this.widget = new OktaSignIn({
            baseUrl: 'https://dev-973041.oktapreview.com',
            clientId: '0oafq6fz6iGe53aZ90h7',
            redirectUri: 'http://localhost:4200',
            authParams: {
                issuer: 'default'
            }
        });
    }
    async setUserStatus() {
        await this.oktaAuthService.getUser()
            .then(user => {
                console.log('the updated signin user', user)
                if (user) {
                    this.userId = user.sub
                    this.getGroups(user.sub).subscribe(groups => {
                        const findAdminGroup = groups.find(group => group.profile.name === 'admin')
                        if (findAdminGroup) this.setAdmin()
                        else this.setUser()
                    })
                }
            })
    }

    getAdminRole(): Observable<boolean> {
        return this.isAdmin
    }

    getUserRole(): Observable<boolean> {
        return this.isUser.asObservable()
    }

    getUserId(): Observable<string> {
        return this.userId
    }

    getGroups(sub): any {
        return this.http.get(`${okta.oktaURL}api/v1/users/${sub}/groups`,
            { headers: this.headers })
            .pipe(
                map(res => { return res }),
                retry(3),
                catchError(this.handleError),
        )
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }

    setAdmin() {
        this.isAdmin.next(true)
        this.isUser.next(false)
    }

    setUser() {
        this.isAdmin.next(false)
        this.isUser.next(true)
    }

    setUserId(id) {
        console.log('IDIDIDIDIDID', id)
        this.userId = id
    }

    getWidget() {
        return this.widget;
    }
}