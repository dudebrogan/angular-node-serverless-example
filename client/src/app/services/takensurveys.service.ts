import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { HttpHeaders, HttpClient, HttpErrorResponse } from "@angular/common/http";
import { throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';

import SurveysUrl from '../../surveysurl'

@Injectable()
export class TakenSurveysService {
    headers;

    constructor(private http: HttpClient, private authService: AuthService) {
        this.headers = new HttpHeaders({
            'Authorization': 'Bearer ' + localStorage.getItem('bearer'),
            'User': this.authService.getUserId().toString()
        })
    }

    getTakenSurveys() {
        console.log('in the service')
        return this.http.get(`${SurveysUrl}surveys/taken`,
            { headers: this.headers })
            .pipe(
                retry(3),
                catchError(this.handleError)
            )
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // window.location.reload(true)
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };

    getOneAnswers(id) {
        return this.http.get(`${SurveysUrl}takensurveys/${id}`,
            { headers: this.headers })
            .pipe(
                retry(3),
                catchError(this.handleError)
            )
    }
}