import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import SurveysUrl from '../../surveysurl'
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { AuthService } from './auth.service';


export interface surveys {
    surveys: Array<Object>
}

@Injectable()
export class ActiveSurveyService {
    headers;

    constructor(private http: HttpClient, private authService: AuthService) {
        this.headers = new HttpHeaders({
            'Authorization': 'Bearer ' + localStorage.getItem('bearer'),
            'User': this.authService.getUserId().toString(),
            'Admin': this.authService.isAdmin.value.toString()
        })
    }

    getActiveSurveys() {
        console.log(this.authService.isAdmin)
        return this.http.get(`${SurveysUrl}surveys/active`,
            { headers: this.headers })
            .pipe(
                retry(3),
                catchError(this.handleError)
            )
    }

    postActiveSurvey(survey) {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + localStorage.getItem('headers')
        })
        return this.http.post(`${SurveysUrl}surveys`, JSON.stringify(survey), { headers })
            .pipe(
                retry(3),
                catchError(this.handleError)
            )
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };
}