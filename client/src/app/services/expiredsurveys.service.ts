import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import SurveysUrl from '../../surveysurl'
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


export interface surveys {
    surveys: Array<Object>
}

@Injectable()
export class ExpiredSurveyService {

    constructor(private http: HttpClient) { }

    getExpiredSurveys() {
        return this.http.get(SurveysUrl + 'surveys/expired')
            .pipe(
                retry(3),
                catchError(this.handleError)
            )
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };


}