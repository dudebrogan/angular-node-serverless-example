import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyresponsesComponent } from './surveyresponses.component';

describe('SurveyresponsesComponent', () => {
  let component: SurveyresponsesComponent;
  let fixture: ComponentFixture<SurveyresponsesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyresponsesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyresponsesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
