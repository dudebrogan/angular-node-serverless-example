import { Component } from '@angular/core'
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms'
import { ActiveSurveyService } from '../services/activesurveys.service'
import { questionTypes, answerFormat, Survey, Question } from '../models/surveyModels'

@Component({
  selector: 'app-addsurvey',
  templateUrl: './addsurvey.component.html',
  styleUrls: ['./addsurvey.component.scss'],
  providers: [ActiveSurveyService]
})
export class AddsurveyComponent {
  surveyForm: FormGroup
  survey: {}
  questionTypes = questionTypes
  surveyEdited = false
  dateError = false
  surveyWillBeActive = false
  surveyCreated = false

  constructor(private fb: FormBuilder, private activeSurveyService: ActiveSurveyService) {
    this.createForm()
  }

  updateDates() {
    const end = this.surveyForm.controls['expiresDate'].value
    const start = this.surveyForm.controls['startDate'].value
    const expiresDate = end == null ? null : new Date(end)
    const startDate = start == null ? null : new Date(start)

    if (expiresDate != null && expiresDate < startDate) {
      this.dateError = true
    } else { this.dateError = false }

    if (startDate != null && Date.now() > startDate.getTime()) {
      this.surveyWillBeActive = true
    } else this.surveyWillBeActive = false
  }

  createForm() {
    this.surveyForm = this.fb.group({
      name: [null, Validators.required],
      startDate: [null, Validators.required],
      expiresDate: [null, Validators.required],
      questions: this.fb.array([this.setQuestion()])
    })

    this.updateControls()

    this.surveyForm.valueChanges.subscribe(data => {
      this.surveyEdited = true
      this.updateControls()
    })
    this.surveyForm.get('startDate').valueChanges.subscribe(date => {
      this.updateDates()
    })

    this.surveyForm.get('expiresDate').valueChanges.subscribe(date => {
      this.surveyForm.value.expiresDate = Date.parse(date)
      this.updateDates()
    })
  }

  updateControls() {
    this.surveyForm.get('questions')['controls'].forEach(control => {
      control.controls.questionType.valueChanges.subscribe(question => {
        let answers = control.controls.possibleAnswers

        console.log('testing')

        answers.controls.splice(0)

        switch (question) {
          case "True/False":
            if (answers.length < 2) {
              answers.push(this.setAnswer('True'))
              answers.push(this.setAnswer('False'))
            }
            break
          case "Multiple Choice":
            answers.push(this.setAnswer(''))
            answers.push(this.setAnswer(''))
            break
          case "Free-Form":
            answers.push(this.setAnswer(' '))
            break
          case "Yes/No":
            answers.push(this.setAnswer('Yes'))
            answers.push(this.setAnswer('No'))
            break
          case "Choose All That Apply":
            answers.push(this.setAnswer(''))
            answers.push(this.setAnswer(''))
            break
        }
      })
    })
  }

  setQuestion() {
    let question = this.fb.group({
      title: ['', Validators.required],
      questionType: ['', Validators.required],
      possibleAnswers: this.fb.array([])
    })

    return question
  }

  addQuestion() {
    (<FormArray>this.surveyForm.get('questions')).push(this.setQuestion())
  }

  removeQuestion(i) {
    this.questions.removeAt(i)
  }

  setAnswer(value) {
    let answer = this.fb.group({
      text: [value, Validators.required]
    })

    return answer
  }

  addAnswer(answers: any) {
    answers.push(this.setAnswer(''))
  }

  removeAnswer(answers, i) {
    answers.removeAt(i)
  }

  get questions(): FormArray {
    return <FormArray>this.surveyForm.get('questions')
  }

  submitSurvey() {
    const survey = this.prepSaveSurvey()
    this.activeSurveyService.postActiveSurvey(survey)
      .subscribe(res => {
        if (res) this.surveyCreated = true
        setTimeout(() => this.surveyCreated = false, 5000)
        this.revert()
        this.surveyWillBeActive = false
      })
  }

  prepSaveSurvey() {
    const formValue = this.surveyForm.value

    const questionsCopy: Question[] = formValue.questions.map(
      (question: Question) => Object.assign({}, question)
    )

    const saveSurvey: Survey = {
      name: formValue.name,
      created: Date.now(),
      startDate: Date.parse(formValue.startDate),
      expiresDate: Date.parse(formValue.expiresDate),
      questions: questionsCopy
    }
    return (saveSurvey)
  }

  revert() {
    this.surveyForm.reset()
  }
}
