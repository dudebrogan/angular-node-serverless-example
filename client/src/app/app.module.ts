import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppRoutes } from './app.routes';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { SplashComponent } from './splash/splash.component';
import { HeaderComponent } from './components/header/header.component';
// import { ActivesurveysComponent } from './activesurveys/activesurveys.component';
// import { EndedsurveysComponent } from './endedsurveys/endedsurveys.component';
// import { QuestionsDialog } from './components/surveyquestions/surveyquestions.component';
import { OktaAuthModule, OktaCallbackComponent } from '@okta/okta-angular';
import { AdminModule } from './admin/admin.module';

import { MatDialogModule, MatListModule, MatProgressSpinnerModule, MatCardModule, MatCheckboxModule, MAT_CHECKBOX_CLICK_ACTION, MatButtonModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatSelectModule } from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SortablejsModule } from 'angular-sortablejs/dist'
import { ReactiveFormsModule } from '@angular/forms'
// import { UnactivatedsurveysComponent } from './unactivatedsurveys/unactivatedsurveys.component';
// import { AddsurveyComponent } from './addsurvey/addsurvey.component';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';
import { AdminAuthGuard } from './services/admin.auth.service';
import { TakeSurveyComponent } from './take-survey/take-survey.component';
import { UserAuthGuard } from './services/user.auth.service';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { FooterComponent } from './footer/footer.component';
import { SurveyanswersComponent } from './components/surveyanswers/surveyanswers.component';
import { SurveyresponsesComponent } from './surveyresponses/surveyresponses.component';
// import { AdminComponent } from './admin/admin.component';


const config = {
  issuer: 'https://dev-973041.oktapreview.com/oauth2/default',
  redirectUri: 'http://localhost:4200/implicit/callback',
  clientId: '0oafq6fz6iGe53aZ90h7'
}

@NgModule({
  declarations: [
    AppComponent,
    SplashComponent,
    HeaderComponent,
    LoginComponent,
    FooterComponent,
    SurveyanswersComponent,
    SurveyresponsesComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDialogModule,
    MatButtonModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatListModule,
    MatCardModule,
    MatProgressSpinnerModule,
    SortablejsModule.forRoot({ animation: 150 }),
    OktaAuthModule.initAuth(config),
    ScrollToModule.forRoot(),
    AppRoutes,
  ],
  entryComponents: [
    // QuestionsDialog
  ],
  providers: [
    { provide: MAT_CHECKBOX_CLICK_ACTION, useValue: {} },
    AdminAuthGuard,
    AuthService,
    UserAuthGuard
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
