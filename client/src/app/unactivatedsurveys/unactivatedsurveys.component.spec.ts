import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnactivatedsurveysComponent } from './unactivatedsurveys.component';

describe('UnactivatedsurveysComponent', () => {
  let component: UnactivatedsurveysComponent;
  let fixture: ComponentFixture<UnactivatedsurveysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnactivatedsurveysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnactivatedsurveysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
