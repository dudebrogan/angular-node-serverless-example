import { Component, OnInit, Inject } from '@angular/core';
import { UnactivatedSurveyService, surveys } from '../services/unactivatedsurveys.service'
import { SurveysService } from '../services/surveys.service';

import { MatDialog } from '@angular/material';
import { QuestionsDialog } from '../components/surveyquestions/surveyquestions.component'

@Component({
  selector: 'app-unactivatedsurveys',
  templateUrl: './unactivatedsurveys.component.html',
  styleUrls: ['./unactivatedsurveys.component.scss'],
  providers: [UnactivatedSurveyService, SurveysService]
})
export class UnactivatedsurveysComponent implements OnInit {
  unactivatedSurveys: any[] = []

  constructor(private unactivatedSurveyService: UnactivatedSurveyService, public dialog: MatDialog, private surveysService: SurveysService) { }

  ngOnInit() {
    this.getSurveys()
  }

  openDialog(survey): void {
    let x = []
    this.dialog.open(QuestionsDialog, {
      width: '1000px',
      data: survey.questions
    });
  }

  activateSurvey(surveyId) {
    console.log(surveyId)
    this.unactivatedSurveyService.activateSurvey(surveyId)
      .subscribe(data => {
        console.log(data)
        this.getSurveys()
      })
  }

  deleteSurvey(surveyId) {
    console.log(surveyId)
    this.surveysService.deleteSurvey(surveyId)
      .subscribe(data => {
        console.log(data)
        this.getSurveys()
      })
  }

  getSurveys() {
    this.unactivatedSurveyService.getUnactivatedSurveys()
      .subscribe((data: any[]) => {
        let formattedData = data.map(survey => {
          survey.created = new Date(survey.created).toDateString()
          survey.startDate = new Date(survey.startDate).toDateString()
          survey.expiresDate = new Date(survey.expiresDate).toDateString()
          survey.questions = survey.questions.map(question => {
            question.possibleAnswers = question.possibleAnswers.map(answer => answer.text)
            return question
          })
          return survey
        })
        this.unactivatedSurveys = formattedData
      });
  }
}