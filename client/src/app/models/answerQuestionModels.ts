export class QuestionBase<T> {
  value: string;
  _id: string;
  title: string;
  required: boolean;
  questionType: string;
  possibleAnswers: any[];

  constructor(options: {
    value?: T,
    _id?: string,
    title?: string,
    required?: boolean,
    questionType?: string
    possibleAnswers?: any[]
  } = {}) {
    this.value = '';
    this._id = options._id || '';
    this.title = options.title || '';
    this.required = true
    this.questionType = options.questionType || '';
    this.possibleAnswers = options.possibleAnswers || [];
  }
}

export class FreeFormQuestion extends QuestionBase<string> {
  controlType = 'Free-Form';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}


export class MultipleChoiceQuestion extends QuestionBase<string> {
  controlType = 'multipleChoice';
  options: string[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}

export class TrueFalseQuestion extends QuestionBase<string> {
  controlType = 'trueFalse';
  options: string[] = [];

  constructor(options: {} = {}) {
    super(options)
    this.options = options['options'] || ['True', 'False']
  }
}

export class ChooseAllThatApplyQuestion extends QuestionBase<string> {
  controlType = 'chooseAllThatApply';
  options: string[] = [];

  constructor(options: {} = {}) {
    super(options)
    this.options = options['options'] || [];
  }
}

export class YesNoQuestion extends QuestionBase<string> {
  controlType = 'yesNo';
  options: string[] = [];

  constructor(options: {} = {}) {
    super(options)
    this.options = options['options'] || ['Yes', 'No']
  }
}