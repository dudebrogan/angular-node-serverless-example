import { Validators } from '@angular/forms';

export const questionTypes = ['Multiple Choice', 'True/False', 'Free-Form', 'Yes/No', 'Choose All That Apply']

export const questionFormat = {
    title: ['', Validators.required],
    questionType: ['', Validators.required],
    possibleAnswers: []
}

export class Survey {
    name = ''
    created = 0
    startDate = 0
    expiresDate = 0
    questions: Question[]
}

export class Question {
    title = ''
    questionType = ''
    possibleAnswers = []
}

export const answerFormat = {
    text: ['', Validators.required]
}

