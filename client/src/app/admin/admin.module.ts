import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { ActivesurveysComponent } from '../activesurveys/activesurveys.component';
import { EndedsurveysComponent } from '../endedsurveys/endedsurveys.component';
import { UnactivatedsurveysComponent } from '../unactivatedsurveys/unactivatedsurveys.component';
import { AddsurveyComponent } from '../addsurvey/addsurvey.component';
import { AdminComponent } from './admin.component';
import { QuestionsDialog } from '../components/surveyquestions/surveyquestions.component';
import { MatDialogModule, MatCheckboxModule, MAT_CHECKBOX_CLICK_ACTION, MatButtonModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatSelectModule } from '@angular/material'
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SortablejsModule } from 'angular-sortablejs/dist'

import { AdminRoutes } from './admin.routes'
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@NgModule({
    declarations: [
        AdminComponent,
        ActivesurveysComponent,
        EndedsurveysComponent,
        UnactivatedsurveysComponent,
        AddsurveyComponent,
        QuestionsDialog,
    ],
    exports: [
        RouterModule,
        ActivesurveysComponent,
        EndedsurveysComponent,
        UnactivatedsurveysComponent,
        AddsurveyComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatDialogModule,
        MatButtonModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule,
        MatSelectModule,
        MatCheckboxModule,
        SortablejsModule.forRoot({ animation: 150 }),
        AdminRoutes
    ],
    entryComponents: [
        QuestionsDialog
    ],
    providers: [
        { provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'noop' },
    ]
})

export class AdminModule { }