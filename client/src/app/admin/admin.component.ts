import { Component } from '@angular/core'
import { AdminAuthGuard } from '../services/admin.auth.service'

@Component({
  template: `
    <router-outlet></router-outlet>
  `,
  providers: [
    AdminAuthGuard
  ]
})
export class AdminComponent {
}