import { Routes, RouterModule } from '@angular/router'
import { AdminAuthGuard } from '../services/admin.auth.service'

import { AdminComponent } from './admin.component'
import { ActivesurveysComponent } from '../activesurveys/activesurveys.component'
import { EndedsurveysComponent } from '../endedsurveys/endedsurveys.component'
import { UnactivatedsurveysComponent } from '../unactivatedsurveys/unactivatedsurveys.component'
import { AddsurveyComponent } from '../addsurvey/addsurvey.component'
import { NgModule } from '@angular/core'

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AdminAuthGuard],
        children: [
            {
                path: '',
                children: [
                    { path: 'activesurveys', component: ActivesurveysComponent },
                    { path: 'unactivatedsurveys', component: UnactivatedsurveysComponent },
                    { path: 'endedsurveys', component: EndedsurveysComponent },
                    { path: 'addsurvey', component: AddsurveyComponent },
                    { path: '', pathMatch: 'full', redirectTo: 'activesurveys' },
                ]
            }
        ]
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AdminRoutes { }