import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { SurveyanswersComponent } from './surveyanswers.component'

describe('SurveyanswersComponent', () => {
  let component: SurveyanswersComponent
  let fixture: ComponentFixture<SurveyanswersComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SurveyanswersComponent]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyanswersComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
