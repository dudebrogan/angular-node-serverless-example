import { Component, OnInit, Inject } from '@angular/core'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'

@Component({
  selector: 'app-surveyanswers',
  templateUrl: './surveyanswers.component.html',
  styleUrls: ['./surveyanswers.component.scss']
})
export class SurveyanswersComponent implements OnInit {
  displayedColumns: string[] = ['title', 'answers']

  constructor(
    public dialogRef: MatDialogRef<SurveyanswersComponent>,
    @Inject(MAT_DIALOG_DATA) public dataSource: any[]) { }

  ngOnInit() {
    console.log(this.dataSource)
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

}
