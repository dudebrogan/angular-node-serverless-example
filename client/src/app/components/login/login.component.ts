import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { OktaAuthService } from '@okta/okta-angular';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import OktaSignIn from '@okta/okta-signin-widget/dist/js/okta-sign-in-no-jquery';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  title = `Nathan's Surveys`
  oktaSignIn
  widget
  @Output() isAuthenticated = new EventEmitter()


  constructor(private authService: AuthService, private changeDetectorRef: ChangeDetectorRef, public oktaAuthService: OktaAuthService) {
    this.oktaSignIn = oktaAuthService
    this.widget = authService.getWidget()
  }

  showLogin() {
    // this.oktaSignIn.renderEl({ el: '#okta-login-container' }, (response) => {
    //   if (response.status === 'SUCCESS') {
    //     this.oktaSignIn.remove();
    //     this.changeDetectorRef.detectChanges();
    //   }
    // });
    console.log('show login', this.widget)
    this.widget.renderEl({
      el: '#login-container'
    },
      (res) => {
        console.log('fuck')
        if (res.status === 'SUCCESS') {
          this.oktaSignIn.loginRedirect('/', { sessionToken: res.session.token });
          // Hide the oktaSignIn
          this.widget.hide();
        }
      },
      (err) => {
        console.log('err fuck')
        throw err;
      }
    );
  }

  ngOnInit() {
    console.log('test2')
    this.widget.session.get((response) => {
      if (response.status !== 'INACTIVE') {
        console.log('test3', response)
        this.isAuthenticated.emit(true)
        // this.changeDetectorRef.detectChanges();
      } else {
        console.log('test5', response)
        // this.isAuthenticated.emit(false)
        this.showLogin();
      }
    });
  }

  logout() {
    this.oktaSignIn.signOut(() => {
      // this.changeDetectorRef.detectChanges();
      this.showLogin();
    });
  }

}
