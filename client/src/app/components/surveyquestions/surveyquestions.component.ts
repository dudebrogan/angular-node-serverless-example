import { Component, Inject, OnInit } from '@angular/core'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: './surveyquestions.component.html'
})

export class QuestionsDialog {
  // data = questions
  displayedColumns: string[] = ['question', 'possibleAnswers', 'questionType']

  constructor(
    public dialogRef: MatDialogRef<QuestionsDialog>,
    @Inject(MAT_DIALOG_DATA) public dataSource: any[]) { }

  ngOnInit() {
    console.log(MAT_DIALOG_DATA)
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

}