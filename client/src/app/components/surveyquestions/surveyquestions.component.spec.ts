import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { QuestionsDialog } from './surveyquestions.component'

describe('QuestionsDialog', () => {
  let component: QuestionsDialog
  let fixture: ComponentFixture<QuestionsDialog>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuestionsDialog]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsDialog)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
