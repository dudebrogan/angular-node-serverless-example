import { Component, OnInit, Input } from '@angular/core'
import { OktaAuthService } from '@okta/okta-angular'
import { AuthService } from '../../services/auth.service'
import { Observable, of } from 'rxjs'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  admin: boolean
  user: boolean
  @Input() isAuthenticated

  constructor(public oktaAuthService: OktaAuthService, private authService: AuthService) { }

  ngOnInit() {
    this.authService.getAdminRole().subscribe(x => this.admin = x)
    this.authService.getUserRole().subscribe(x => this.user = x)
  }

  logout() {
    this.oktaAuthService.logout('/')
    this.admin = false
    this.user = false
    this.isAuthenticated = false
  }

  login() {
    this.oktaAuthService.loginRedirect('/')
  }

}
